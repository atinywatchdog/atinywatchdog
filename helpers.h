/*
ATinyWatchdog, firmware to use an ATTiny as a hardware watchdog IC
Copyright (C) 2020, Sean Lanigan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/io.h>

#define PIN_RST_A ( 1 << PORTB0 )
#define PIN_RST_B ( 1 << PORTB1 )
#define PIN_WD_IN ( 1 << PORTB2 )
#define PIN_RST_E ( 1 << PORTB3 )

#define VLM_MODE_2 ( 1 << VLM1 | 1 << VLM0 )
#define VLM_IE ( 1 << VLMIE )

#define MASK_4BIT 0b00001111

#define WD_IN_SETTLE_TIME 25
