/*
ATinyWatchdog, firmware to use an ATTiny as a hardware watchdog IC
Copyright (C) 2020, Sean Lanigan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

// 1 MHz CPU speed is the default
#define F_CPU 1000000UL
#include <util/delay.h>

#include "helpers.h"


// WDT must be disabled on boot, or a boot loop will occur. The "init3" section runs 
// very early in the boot process, using "main()" will be too slow and violate the 
// 15 ms default watchdog timeout - causing another reboot and hence a boot loop. 
void disable_wdt(void) __attribute__((naked)) __attribute__((section(".init3")));
void disable_wdt(void)
{
    // Disable interrupts while we configure things, it will be enabled later on
    cli();
    // RSTFLR must be cleared to allow disabling watchdog timer
    RSTFLR = 0;
    wdt_disable();
}


int main(void)
{
    // Set RST_A and RST_B as output, and WD_IN and RST_E as input
    DDRB = (~(PIN_WD_IN | PIN_RST_E) & (PIN_RST_A | PIN_RST_B)) & MASK_4BIT;
    // Set RST_A and RST_B to logic low (i.e. drive reset lines). Since WD_IN and RST_E
    // are set as inputs then their PORTB setting is ignored (and defaults to 0 anyway)
    PORTB = 0;
    // Enable pullup for WD_IN
    PUEB = PIN_WD_IN;

    // Enable the VCC level monitor at setting VLM2, with VLMIE interrupt disabled
    // VLMCSR = VLM_MODE_2;

    // Turn off Timer0, to save some power
    PRR = (1 << PRTIM0);

    // Sleep for a second on power-up, holds output in reset
    _delay_ms(1000);

    // Stop pulling RST_A low
    DDRB &= ~PIN_RST_A;

    _delay_ms(50);

    // Stop pulling RST_B low
    DDRB &= ~PIN_RST_B;

    // Configure WDT to 2 seconds
    wdt_enable(WDTO_2S);

    // Enable interrupts
    // sei();

    // Store initial WD_IN value
    uint8_t wd_in_now = PINB & PIN_WD_IN;
    uint8_t wd_in_last = wd_in_now;

    // Hold here until until first WD_IN trigger
    while (true)
    {
        // Check WD_IN value
        wd_in_last = wd_in_now;
        wd_in_now = PINB & PIN_WD_IN;

        if (wd_in_last != wd_in_now)
        {
            // If WD_IN has been triggered, stop waiting
            _delay_ms(100);
            break;
        }
        else
        {
            wdt_reset();
        }
    }

    // Continually poll the WD_IN pin for changes, reset WDT if pin has changed
    while (true)
    {
        wd_in_now = PINB & PIN_WD_IN;

        if (wd_in_last != wd_in_now)
        {
            // Make sure WD_IN stays in the new state for WD_IN_SETTLE_TIME ms
            uint8_t count = 0;
            while (true)
            {
                _delay_ms(1);
                if (wd_in_now == (PINB & PIN_WD_IN))
                {
                    // Value has stayed at the new level, count this millisecond
                    ++count;
                    // If enough successful periods at new value, perform WDT reset
                    if (count > (WD_IN_SETTLE_TIME - 1))
                    {
                        wdt_reset();
                        wd_in_last = wd_in_now;
                        break;
                    }
                }
                else
                {
                    // Value has not stayed at the new level, do not reset WDT
                    break;
                }
            }
        }
    }
}
