# ATinyWatchdog

A hardware watchdog designed for embedded Linux systems


## Introduction

The purpose of this ATinyWatchdog firmware is to use a cheap ATTiny4 microcontroller to function as an external hardware watchdog and voltage monitor IC, comparable to parts like the STWD100, MAX6814, XC6121, or ADM8320 -- but at a lower price.


## Features

To uniquely cater for embedded Linux systems, which typically have longer power-on boot cycles than bare metal systems, the watchdog timer will not start running until the first trigger is detected on the WD_IN pin -- this is to allow time for the Linux system to initialise before it must then reset the watchdog.

The voltage level monitoring of the ATTiny4 is also used to ensure brown-outs trigger a proper system reset.

Finally, two reset outputs are provided, to facilitate power-up sequencing where required.

The reset outputs are configured as open-drain, meaning external pull-ups are required. Push-pull mode is also possible by modifying the firmware.


## License

This project is released under the terms of the GNU GPL version 3 (or
later). Please see [LICENSE](LICENSE) for details.
