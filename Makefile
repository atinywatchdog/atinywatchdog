PRG	    = main
OBJ	    = main.o
MCU_TARGET     = attiny9
OPTIMIZE       = -O3

DEFS	   =
LIBS	   =

CC	     = avr-gcc

# Override is only needed by avr-lib build system.

override CFLAGS	 = -g -Wall -Wextra $(OPTIMIZE) -mmcu=$(MCU_TARGET) $(DEFS)
override LDFLAGS = -Wl,-Map,$(PRG).map

OBJCOPY	= avr-objcopy
OBJDUMP	= avr-objdump
OBJSIZE = avr-size

all: $(PRG).elf lst text size

$(PRG).elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

# dependency:
main.o: main.c

clean:
	rm -rf *.o $(PRG).elf *.eps *.png *.pdf *.bak *.lst *.map *.hex *.bin *.srec

lst:  $(PRG).lst

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

# Rules for building the .text rom images

text: hex bin srec

hex:  $(PRG).hex
bin:  $(PRG).bin
srec: $(PRG).srec

%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

# Rules for printing out the memory usage after a build

size: 
	$(OBJSIZE) --mcu=$(MCU_TARGET) -C $(PRG).elf

